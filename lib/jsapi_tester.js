// Copyright (c) 2016 Dell Boomi, Inc.

var http = require('http');
var fs = require('fs');
var connect = require('connect');

var resourcedir = fs.realpathSync(__dirname+"/../tester");
var boomiapidir = fs.realpathSync(__dirname+"/../node_modules/BoomiAPI/js");
var swaggerparserdir = fs.realpathSync(__dirname+"/../node_modules/swagger-parser/dist");

var app = connect()
	.use(connect.static(resourcedir))
	.use(connect.static(swaggerparserdir))
	.use(connect.static(boomiapidir));
	
var server = http.createServer(app).listen(8080);
console.log("Boomi JS API Tester started on port ["+8080+"]");
