// Copyright (c) 2016 Dell Boomi, Inc.

define(['jquery', 'BoomiAPI', "APIObjectGenerator", 'jqueryui'], function($, BoomiAPI, APIObjectGenerator) {
	var optionsStr = localStorage["jsapitester.options"];
	var options = {
		userid: "",
		password: "",
		accountId: "",
		baseUrl: ""
	};
	if (optionsStr) {
		options = JSON.parse(optionsStr);
		if (options.baseUrl.charAt(options.baseUrl.length-1) === "/") {
			options.baseUrl = options.baseUrl.substring(0, options.baseUrl.length-1);
		}
	}

	var apiObjectTypes = [];
	APIObjectGenerator.getAPIObjectTypes().then(function(types) {
		apiObjectTypes = types;
	});
	
	var apiObjectName = "Account";
	var apiObject;
	var apiAction;
	
	function handleResults(results) {
		$("#output").val(JSON.stringify(results, null, '\t'));
	}
	
	function handleError(error) {
		$("#output").val(JSON.stringify(error, null, '\t'));
	}
	
	function loadActions() {
		$("#action").selectmenu( "disable" );
		if (apiObject) {
			$("#action").selectmenu( "enable" );
			$("#action").empty();
			var actions = [];
			if (apiObject.query) {
				actions.push("<option>Query</option>");
				actions.push("<option>Query More</option>");
			}
			if (apiObject.get) {
				actions.push("<option>Get</option>");
			}
			if (apiObject.create) {
				actions.push("<option>Create</option>");
			}
			if (apiObject.update) {
				actions.push("<option>Update</option>");
			}
			if (apiObject.del) {
				actions.push("<option>Delete</option>");
			}
			if (apiObject.execute) {
				actions.push("<option>Execute</option>");
			}
			if (apiObject.bulk) {
				actions.push("<option>Bulk</option>");
			}
			$("#action").append(actions.join(""));
			$("#action").selectmenu("refresh");
		}
	}
	
	function showHideInput(value) {
		$("#input").text("");
		switch (value) {
			case "Query":
				$("#idContainer").hide();
				$("#inputContainer").show();
				$("#input").text("{\"QueryFilter\" : {\"expression\" : {}}}");
				break;
			case "Get":
				$("#idContainer").show();
				$("#inputContainer").hide();
				break;
			case "Query More":
				$("#idContainer").hide();
				$("#inputContainer").show();
				break;
			case "Create":
				$("#idContainer").hide();
				$("#inputContainer").show();
				if (apiObjectName && apiObjectTypes.indexOf(apiObjectName) !== -1) {
					APIObjectGenerator.generateAPIObject(apiObjectName).then(function(obj) {
						$("#input").text(JSON.stringify(obj, null, '\t'));
					}).catch(function(err) {
						console.log(err);
					});					
				}
				break;
			case "Update":
				$("#idContainer").show();
				$("#inputContainer").show();
				if (apiObjectName && apiObjectTypes.indexOf(apiObjectName) !== -1) {
					APIObjectGenerator.generateAPIObject(apiObjectName).then(function(obj) {
						$("#input").text(JSON.stringify(obj, null, '\t'));
					});					
				}
				break;
			case "Delete":
				$("#idContainer").show();
				$("#inputContainer").hide();
				break;			
			case "Execute":
				$("#idContainer").show();
				$("#inputContainer").show();
				break;			
			case "Bulk":
				$("#idContainer").hide();
				$("#inputContainer").show();
				break;
			default:	
				$("#idContainer").show();
				$("#inputContainer").show();
				break;
		}
	}
	
    $(document).ready(function() {
		$("#idContainer").hide();

		$("#userid").val(options.userid);
		$("#password").val(options.password);
		$("#accountId").val(options.accountId);
		$("#url").val(options.baseUrl);
    
		$("#object" ).selectmenu({
			change: function( event, data ) {
				apiObjectName = data.item.value;
				apiObject = undefined;
				switch (data.item.value) {
					case "Account":
						apiObject = BoomiAPI.account(options);
						break;
					case "AccountGroupAccount":
						apiObject = BoomiAPI.accountGroupAccount(options);
						break;
					case "AccountGroupUserRole":
						apiObject = BoomiAPI.accountGroupUserRole(options);
						break;
					case "AccountGroup":
						apiObject = BoomiAPI.accountGroup(options);
						break;
					case "AccountSSOConfig":
						apiObject = BoomiAPI.accountSSOConfig(options);
						break;
					case "AccountUserFederation":
						apiObject = BoomiAPI.accountUserFederation(options);
						break;
					case "AccountUserRole":
						apiObject = BoomiAPI.accountUserRole(options);
						break;
					case "AccountProvision":
						apiObject = BoomiAPI.accountProvision(options);
						break;
					case "AtomConnectionFieldExtensionSummary":
						apiObject = BoomiAPI.atomConnectionFieldExtensionSummary(options);
						break;
					case "AtomConnectorVersions":
						apiObject = BoomiAPI.atomConnectorVersions(options);
						break;
					case "Environment":
						apiObject = BoomiAPI.environment(options);
						break;
					case "EnvironmentRole":
						apiObject = BoomiAPI.environmentRole(options);
						break;
					case "InstallerToken":
						apiObject = BoomiAPI.installerToken(options);
						break;
					case "IntegrationPack":
						apiObject = BoomiAPI.integrationPack(options);
						break;
					case "IntegrationPackInstance":
						apiObject = BoomiAPI.integrationPackInstance(options);
						break;
					case "IntegrationPackEnvironmentAttachment":
						apiObject = BoomiAPI.integrationPackEnvironmentAttachment(options);
						break;
					case "Cloud":
						apiObject = BoomiAPI.cloud(options);
						break;
					case "Atom":
						apiObject = BoomiAPI.atom(options);
						break;
					case "EnvironmentAtomAttachment":
						apiObject = BoomiAPI.environmentAtomAttachment(options);
						break;
					case "EnvironmentExtensions":
						apiObject = BoomiAPI.environmentExtensions(options);
						break;
					case "AtomExtensions":
						apiObject = BoomiAPI.atomExtensions(options);
						break;
					case "Process":
						apiObject = BoomiAPI.process(options);
						break;
					case "ExecutionRecord":
						apiObject = BoomiAPI.executionRecord(options);
						break;
					case "ProcessSchedules":
						apiObject = BoomiAPI.processSchedules(options);
						break;
					case "AccountUserRole":
						apiObject = BoomiAPI.accountUserRole(options);
						break;
					case "EnvironmentMapExtensionsSummary":
						apiObject = BoomiAPI.environmentMapExtensionsSummary(options);
						break;
					case "AtomMapExtensionsSummary":
						apiObject = BoomiAPI.atomMapExtensionsSummary(options);
						break;
					case "EnvironmentMapExtension":
						apiObject = BoomiAPI.environmentMapExtension(options);
						break;
					case "AtomMapExtension":
						apiObject = BoomiAPI.atomMapExtension(options);
						break;
					case "Event":
						apiObject = BoomiAPI.event(options);
						break;
					case "ExecutionCountAccount":
						apiObject = BoomiAPI.executionCountAccount(options);
						break;
					case "ExecutionCountAccountGroup":
						apiObject = BoomiAPI.executionCountAccountGroup(options);
						break;
					case "ExecutionSummaryRecord":
						apiObject = BoomiAPI.executionSummaryRecord(options);
						break;
					case "IntegrationPackAtomAttachment":
						apiObject = BoomiAPI.integrationPackAtomAttachment(options);
						break;
					case "AtomStartupProperties":
						apiObject = BoomiAPI.atomStartupProperties(options);
						break;
					case "AuditLog":
						apiObject = BoomiAPI.auditLog(options);
						break;
					case "CustomTrackedField":
						apiObject = BoomiAPI.customTrackedField(options);
						break;
					case "Deployment":
						apiObject = BoomiAPI.deployment(options);
						break;
					case "DocumentCountAccount":
						apiObject = BoomiAPI.documentCountAccount(options);
						break;
					case "DocumentCountAccountGroup":
						apiObject = BoomiAPI.documentCountAccountGroup(options);
						break;
					case "EnvironmentConnectionFieldExtensionSummary":
						apiObject = BoomiAPI.environmentConnectionFieldExtensionSummary(options);
						break;
					case "ProcessAtomAttachment":
						apiObject = BoomiAPI.processAtomAttachment(options);
						break;
					case "ProcessEnvironmentAttachment":
						apiObject = BoomiAPI.processEnvironmentAttachment(options);
						break;
					case "ProcessScheduleStatus":
						apiObject = BoomiAPI.processScheduleStatus(options);
						break;
					case "Role":
						apiObject = BoomiAPI.role(options);
						break;
					case "SharedServerInformation":
						apiObject = BoomiAPI.sharedServerInformation(options);
						break;
					case "ThroughputAccount":
						apiObject = BoomiAPI.throughputAccount(options);
						break;
					case "ThroughputAccountGroup":
						apiObject = BoomiAPI.throughputAccountGroup(options);
						break;
					case "WidgetInstanceExtensions":
						apiObject = BoomiAPI.widgetInstanceExtensions(options);
						break;
					case "X12ConnectorRecord":
						apiObject = BoomiAPI.x12ConnectorRecord(options);
						break;
					case "AccountProvision":
						apiObject = BoomiAPI.accountProvision(options);
						break;
					case "ListenerStatus":
						apiObject = BoomiAPI.listenerStatus(options);
						break;
					case "PackagedComponent":
						apiObject = BoomiAPI.packagedComponent(options);
						break;
					case "TradingPartnerComponent":
						apiObject = BoomiAPI.tradingPartnerComponent(options);
						break;
					case "APIUsageCount":
						apiObject = BoomiAPI.apiUsageCount(options);
						break;
					case "getAssignableRoles":
						apiAction = "getAssignableRoles";
						break;
					case "executeProcess":
						apiAction = "executeProcess";
						break;
					case "cancelExecution":
						apiAction = "cancelExecution";
						break;
					case "deployProcess":
						apiAction = "deployProcess";
						break;
					case "deployComponent":
						apiAction = "deployComponent";
						break;
					case "changeListenerStatus":
						apiAction = "changeListenerStatus";
						break;
				}
				loadActions();
				if (apiObject) {
					showHideInput($("#action").val());
				} else {
					showHideInput("");
				}
		  	}
		}).selectmenu( "menuWidget" ).addClass( "overflow" );    
		$("#action" ).selectmenu({
			create: function( event, ui ) {
				loadActions();
			},
			change: function( event, data ) {
				showHideInput(data.item.value);
		  	}
		});
    	$("#run").button().click(function(event) {
    		var action = $("#action").val();
    		var id = $("#id").val();
    		var input = $("#input").val();
    		var overrideId = $("#overrideAccount").val();
    		var params = {};
    		if (overrideId && overrideId !== "") {
    			params.overrideAccountId = overrideId;
    		}
    		if (apiObject) {
				switch (action) {
					case "Query":
						params.filter = JSON.parse(input);
						apiObject.query(params).then(handleResults).fail(handleError);
						break;
					case "Get":
						params.id = id;
						apiObject.get(params).then(handleResults).fail(handleError);
						break;
					case "Query More":
						params.queryToken = input;
						apiObject.query(params).then(handleResults).fail(handleError);
						break;
					case "Create":
						params.data = JSON.parse(input);
						apiObject.create(params).then(handleResults).fail(handleError);
						break;
					case "Update":
						params.id = id;
						params.data = JSON.parse(input);
						apiObject.update(params).then(handleResults).fail(handleError);
						break;
					case "Delete":
						params.id = id;
						apiObject.del(params).then(handleResults).fail(handleError);
						break;			
					case "Execute":
						params.id = id;
						params.data = JSON.parse(input);
						apiObject.execute(params).then(handleResults).fail(handleError);
						break;			
					case "Bulk":
						params.bulk = JSON.parse(input);
						apiObject.bulk(params).then(handleResults).fail(handleError);
						break;			
				}
    		} else {
    			var params = {};
    			switch (apiAction) {
					case "getAssignableRoles":
						BoomiAPI.getAssignableRoles(options, params).then(handleResults).fail(handleError);
						break;
					case "executeProcess":
						params.data = JSON.parse(input);
						BoomiAPI.executeProcess(options, params).then(handleResults).fail(handleError);
						break;
					case "cancelExecution":
						params.id = id;
						BoomiAPI.cancelExecution(options, params).then(handleResults).fail(handleError);
						break;
					case "deployProcess":
						params.id = id;
						BoomiAPI.deployProcess(options, params).then(handleResults).fail(handleError);
						break;
					case "deployComponent":
						params.id = id;
						BoomiAPI.deployComponent(options, params).then(handleResults).fail(handleError);
						break;
					case "changeListenerStatus":
						params.id = id;
						BoomiAPI.changeListenerStatus(options, params).then(handleResults).fail(handleError);
						break;
				}
    		}
      	});		
		$( "#credentialsDialog" ).dialog({
			autoOpen: true,
			height: 300,
			width: 350,
			modal: true,
			buttons: {
				Ok: function() {
					$("#credentialsDialog").dialog( "close" );
					options.userid = $("#userid").val();
					$("#idlabel").text(options.userid);
					options.password = $("#password").val();
					options.accountId = $("#accountId").val();
					$("#accountidlabel").text(options.accountId);
					options.baseUrl = $("#url").val();
					$("#urllabel").text(options.baseUrl);
					localStorage["jsapitester.options"] = JSON.stringify(options);
					apiObject = BoomiAPI.account(options);
					loadActions();
				}
			}
		});
		$( "#input" ).resizable({
		  handles: "se"
		});			                	
		$( "#output" ).resizable({
		  handles: "se"
		});
	});
	
	return {};
});
