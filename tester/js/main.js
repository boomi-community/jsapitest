// Copyright (c) 2016 Dell Boomi, Inc.

require.config({
	baseUrl: 'js/',
	paths: {
		jquery: '//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min',
		jqueryui: '//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min',
		q: '//cdnjs.cloudflare.com/ajax/libs/q.js/1.4.1/q.min',
		SwaggerParser: '../../swagger-parser',
		BoomiAPI: '../../BoomiAPI',
		APIObjectGenerator: '../../APIObjectGenerator'
	}
});

require(['app']);
