## Boomi JS API Testing Tool ##
This is a [Postman](https://www.getpostman.com/docs/introduction) like tool for the [Boomi Platform API](http://help.boomi.com/atomsphere/GUID-C6847C47-5EFF-4933-ADA1-A47D032471C6.html).
### Setup ###
cd into the ***jsapitest*** directory and run
```
#!cmd
npm install
```
### Running the API Testing tool ###
cd into the ***jsapitest*** directory and run

```
#!cmd
node lib/jsapi_tester.js
```

You will see a console message :


```
#!cmd

Boomi JS API Tester started on port [8080]
```


### Usage ###
You can access the Tester via a Browser 

http://localhost:8080

You will be prompted for credentials (including the required Platform URL, typically https://api.boomi.com/ ) and then be able to make API calls

***Note***: This tool makes direct call to the API from a browser and needs CORS authentication setup for http://localhost:8080 in the Boomi Account Security Tab